﻿using Projekti_dev.Repository.Interface;
using System;
using System.Linq;

namespace Projekti_dev
{
    public interface IUnitOfWork
    {
        IDevExpressRepository DevExpress { get; }
    }
}
