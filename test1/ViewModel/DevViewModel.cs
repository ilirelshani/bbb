﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekti_dev.ViewModel
{
    public class DevViewModel
    {
        public int Id { get; set; }
        public string Donatori { get; set; }
        public int Nr_Donatoreve { get; set; }

        
        public int Vlera { get; set; }
        public DateTime Data { get; set; }
        public string Qyteti { get; set; }
        public string Shteti { get; set; }
    }
}
