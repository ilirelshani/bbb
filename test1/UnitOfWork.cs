﻿using Projekti_dev.Repository;
using Projekti_dev.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekti_dev
{
    public class UnitOfWork : IUnitOfWork
    {

        public readonly ApplicationDbContext _context;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }

        IDevExpressRepository _dev;

        public IDevExpressRepository DevExpress
        {
            get
            {
                if (_dev == null)
                    _dev = new DevExpressRepository(_context);
                return _dev;
            }
        }

    }
}
