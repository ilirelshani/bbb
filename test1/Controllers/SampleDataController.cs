using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using test1.Models;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Projekti_dev;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace test1.Controllers {

    [Route("api/[controller]")]
    public class SampleDataController : Controller {


        readonly IUnitOfWork _unitOfWork;
        readonly ApplicationDbContext _context;
        public SampleDataController(IUnitOfWork unitOfWork,ApplicationDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }

        [HttpGet]
        public async Task<object> Get(DataSourceLoadOptions loadOptions) {

            var countDonatoret = _context.DevExpresses.Count();

            var vlera = _context.DevExpresses.Sum(x => x.Vlera);

            ViewBag.donatoret = countDonatoret;

            ViewBag.vlera = vlera;


            var result = await _unitOfWork.DevExpress.GetDevs();
            return DataSourceLoader.Load(result, loadOptions);



        }



        [HttpGet("getForView")]
        public async Task<object> GetForView(DataSourceLoadOptions loadOptions)
        {
            var result = await _unitOfWork.DevExpress.GetDevs();
         
            return DataSourceLoader.Load(result, loadOptions);
        }




        [HttpPost]
        public IActionResult Post(string values)
        {
            var donatoret = new Projekti_dev.Models.DevExpresss();
            JsonConvert.PopulateObject(values, donatoret);

            if (!TryValidateModel(donatoret))
                return BadRequest(ModelState);

            _context.DevExpresses.Add(donatoret);
            _context.SaveChanges();
            return Ok();
        }



        [HttpPut]
        public async Task<IActionResult> Put(int key, string values)
        {
            var employee = await _unitOfWork.DevExpress.GetDevById(key);
            JsonConvert.PopulateObject(values, employee);

            _context.SaveChanges();

            return Ok();
        }


        [HttpDelete]
        public async Task<IActionResult>  Delete(int key)
        {
            var donator = await _unitOfWork.DevExpress.GetDevById(key);
            _context.DevExpresses.Remove(donator);
            _context.SaveChanges();

            return Ok();
        }


    }
}