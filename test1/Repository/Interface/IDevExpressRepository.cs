﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projekti_dev.Repository.Interface
{
    public interface IDevExpressRepository
    {
        Task<IEnumerable<Models.DevExpresss>> GetDevs();
        Task<Models.DevExpresss> GetDevById(int? devId);
        Task CreateDevAsync(Models.DevExpresss devDto);
        Task UpdateDevAsync(Models.DevExpresss devDto);

    }
}
