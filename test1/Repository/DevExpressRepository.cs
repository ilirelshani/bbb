﻿using Microsoft.EntityFrameworkCore;
using Projekti_dev.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projekti_dev.Models;
using System.Globalization;

namespace Projekti_dev.Repository
{
    public class DevExpressRepository : Repository<Models.DevExpresss>, IDevExpressRepository
    {
        public DevExpressRepository(ApplicationDbContext context) : base(context) { }
        private ApplicationDbContext _appContext => (ApplicationDbContext)_context;


        public async Task CreateDevAsync(Models.DevExpresss devDto)
        {
            await _appContext.DevExpresses.AddAsync(devDto);
            await _appContext.SaveChangesAsync();
        }

        public async Task<Models.DevExpresss> GetDevById(int? devId)
        {
            return await _appContext.DevExpresses.FindAsync(devId);
        }

        public async Task<IEnumerable<Models.DevExpresss>> GetDevs()
        {
            IQueryable<Models.DevExpresss> devs;

            devs = (from a in _appContext.DevExpresses
                    select new Models.DevExpresss
                    {
                        Id = a.Id,
                        Qyteti = a.Qyteti,
                        Shteti = a.Shteti,
                        Data = a.Data,
                        Donatori  = a.Donatori,
                        Vlera  = a.Vlera,
                        NrDonatoreve = a.NrDonatoreve,
                        Muaji = getFullName(a.Data.Month),
                        Viti = Convert.ToInt32(a.Data.Year)
                        

        }).OrderByDescending(x=>x.Id).AsQueryable();

            return await devs.ToListAsync();
        }


        static string getFullName(int month)
        {
            var muajt = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);

            muajt = TranslateMonth(muajt);

            return muajt;
        }

        private static string TranslateMonth(string muajt)
        {
            if (muajt.Equals("January"))
            {
                muajt = "Janar";
            }
            if (muajt.Equals("Frebuary"))
            {
                muajt = "Shkurt";
            }

            if (muajt.Equals("March"))
            {
                muajt = "Mars";
            }
            if (muajt.Equals("April"))
            {
                muajt = "Prill";
            }


            if (muajt.Equals("May"))
            {
                muajt = "Maj";
            }

            if (muajt.Equals("June"))
            {
                muajt = "Qershor";
            }
            if (muajt.Equals("July"))
            {
                muajt = "Korrik";
            }

            if (muajt.Equals("August"))
            {
                muajt = "Ngusht";
            }

            if (muajt.Equals("September"))
            {
                muajt = "Shtator";
            }

            if (muajt.Equals("October"))
            {
                muajt = "Tetor";
            }

            if (muajt.Equals("November"))
            {
                muajt = "Nentor";
            }

            if (muajt.Equals("December"))
            {
                muajt = "Dhjetor";
            }

            return muajt;
        }

        public Task UpdateDevAsync(Models.DevExpresss devDto)
        {
            throw new NotImplementedException();
        }
    }
}
