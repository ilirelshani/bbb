﻿using Microsoft.EntityFrameworkCore;
namespace Projekti_dev
{
    public class ApplicationDbContext : DbContext
    {
        public string CurrentUserId { get; set; }
        public DbSet<Models.DevExpresss> DevExpresses { get; set; }
        public ApplicationDbContext(DbContextOptions options) : base(options)
        { }
    }
}
